<?php // $Id$

/**
 * @see peek_summary_views_plugins().
 */
class peek_summary_plugin_style extends views_plugin_style_summary {
  function option_definition() {
    $options = parent::option_definition();
    $options['use_display'] = array('default' => 'default');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $displays = array();
    $link_displays = array();
    foreach ($this->view->display as $key => $display) {
      $displays[$key] = $display->display_title;
      if (is_object($display->handler) && $display->handler->has_path()) {
        $link_displays[$key] = $display->display_title;
      }
    }
    $form['use_display'] = array(
      '#type' => 'radios',
      '#title' => t('Display to use in summary'),
      '#options' => $displays,
      '#default_value' => $this->options['use_display'],
    );
  }

  function render() {
    $argument = $this->view->argument[$this->view->build_info['summary_level']];

    $rows = array();
    foreach ($this->view->result as $row) {
      $view = $this->view->clone_view();
      $view->set_display($this->options['use_display']);
      $view->set_current_page(0);

      $args = $view->args;
      $args[$argument->position] = $argument->summary_argument($row);
      $view->set_arguments($args);

      $view->execute_display();
      $row->peek_summary_view = $view;

      $rows[] = $row;
    }

    return theme($this->theme_functions(), array(
      'view' => $this->view,
      'options' => $this->options,
      'rows' => $rows,
    ));
  }
}
