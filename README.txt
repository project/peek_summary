This is a Views summary style plugin. It allows you to embed the results of a View with each
argument in the argument summary. You may use a different display; for example, you might build an
archive View with a Year+Month argument, that lists the four most popular nodes for each month in
the summary, but that lists nodes in date order when you click through to the argument.

Usage:
* Set the "argument summary" style to "Peek Summary":
  http://img.skitch.com/20100902-8xyryc7pwkr6d8araq35u1pmtt.png

* Configure the style (choose a display plugin, set paging):
  http://img.skitch.com/20100902-pp76fksnn52y6dx84udcg7k3jt.png

* Example peek_summary output:
  http://img.skitch.com/20100902-1m3hupyhdum2472sj9cf78fpks.png

* The same View, with an argument (not peek_summary output):
  http://img.skitch.com/20100902-xmd72pb86k9aku3hej9y3farwa.png

Caveats:
* You should limit the number of arguments displayed per page (in the argument summary). A View is
  executed for each argument that is rendered.

* You should use Views' caching, at least for the display you use for peek_summary.

* The peek_summary display must have the same arguments as the argument summary display--they may or
  may not be the same actual display.

----
This module is developed by Palantir.net (http://palantir.net/).
