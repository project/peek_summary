<?php // $Id$ ?>

<?php foreach ($rows as $id => $row): ?>
  <div<?php print !empty($classes[$id]) ? ' class="'. $classes[$id] .'"' : ''; ?>>
    <h3 class="peek-summary-link">
      <a href="<?php print $row->url; ?>"><?php print $row->link; ?></a>
      <?php if (!empty($options['count'])): ?>
        (<?php print $row->count; ?>)
      <?php endif; ?>
    </h3>
    <div class="peek-summary-content"><?php print $row->peek_summary; ?></div>
  </div>
<?php endforeach; ?>
