<?php // $Id$

/**
 * Implementation of hook_views_plugins().
 *
 * Provide a summary style plugin that displays the results of each summary. At first glance this
 * may look like row grouping, but it allows you to do things like show a list of top posts from
 * each month, while linking to a date-ordered archive View.
 */
function peek_summary_views_plugins() {
  return array(
    'style' => array(
      'peek_summary' => array(
        'title' => t('Peek Summary'),
        'help' => t('Displays the argument link, followed by the results of a display using that argument.'),
        'handler' => 'peek_summary_plugin_style',
        'parent' => 'default_summary',
        'theme' => 'peek_summary',
        'type' => 'summary', // This will only show up as an argument summary style.
        'uses options' => TRUE,
        'help topic' => 'style-summary',
      ),
    ),
  );
}
